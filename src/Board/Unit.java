public class Unit {
    public Unit() {}
    private int hp;
    protected boolean isDestroyed = false;

    public void destroy() { isDestroyed = true; }
    public boolean getIfDestroyed() {
        return isDestroyed;
    }

    public void setDestroyed(boolean destroyed) {
        isDestroyed = destroyed;
    }
}
